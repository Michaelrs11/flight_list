import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flightlist/FlightParts/BottomPart.dart';
import 'package:flightlist/FlightParts/TopPart.dart';
import 'package:flutter/material.dart';

final Color discountBackgroundColor = Color(0xFFFFE08D);
final Color flightBorderColor = Color(0xFFE6E6E6);
final Color chipBackgroundColor = Color(0xFFF6F6F6);

class FlightListingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Search Results",
        ),
        centerTitle: true,
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            FlightListingTopPart(),
            SizedBox(height: 20.0),
            FlightListingBottomPart(),
          ],
        ),
      ),
    );
  }
}

class FlightDetails{
  final String airlines, date, discount, rating;
  final int oldPrice, newPrice;

  FlightDetails.fromMap(Map<String, dynamic> map)
    : assert(map['airlines'] != null),
      assert(map['date'] != null),
      assert(map['discount'] != null),
      assert(map['rating'] != null),
      airlines = map['airlines'],
      date = map['date'],
      discount = map['discount'],
      oldPrice = map['oldPrice'],
      newPrice = map['newPrice'],
      rating = map['rating'];
      
  FlightDetails.fromSnapShot(DocumentSnapshot snapshot): this.fromMap(snapshot.data);


}