import 'package:flightlist/Cards/FlightCard.dart';
import 'package:flightlist/ScreenParts/FlightListScreen.dart';
import 'package:flightlist/ScreenParts/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class FlightListingBottomPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              "Best Deals for next 6 Months",
              style: dropDownMenuItemStyle,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          StreamBuilder(
            stream: Firestore.instance.collection('deals').snapshots(),
            builder: (context, snapshot){
              return !snapshot.hasData
                ? CircularProgressIndicator()
                : buildDealList(context,snapshot.data.documents);
            },
          ),
        ],
      ),
    );
  }
}

Widget buildDealList(BuildContext context, List<DocumentSnapshot> snapshot) {
  return ListView.builder(
    shrinkWrap: true,
    itemCount: snapshot.length,
    physics: ClampingScrollPhysics(),
    scrollDirection: Axis.vertical,
    itemBuilder: (context, index){
      return FlightCard(flightDetails: FlightDetails.fromSnapShot(snapshot[index]),);
    });
}
