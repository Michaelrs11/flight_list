import 'package:flutter/material.dart';

class InheritedFlightListing extends InheritedWidget {
  final String fromLocation, toLocation;

  InheritedFlightListing({this.fromLocation, this.toLocation, Widget child}) : super(child:child);

  static InheritedFlightListing of(BuildContext context) => context.dependOnInheritedWidgetOfExactType<InheritedFlightListing>();

  @override
  bool updateShouldNotify( InheritedFlightListing oldWidget) {
    return true;
  }
}