import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flightlist/ChipParts/ChoiceChip.dart';
import 'package:flightlist/CustomParts/CustomShapeClipper.dart';
import 'package:flightlist/FlightParts/InheritedFlightList.dart';
import 'package:flightlist/ScreenParts/FlightListScreen.dart';
import 'package:flightlist/ScreenParts/HomeScreen.dart';
import 'package:flutter/material.dart';

class HomeScreenTopPart extends StatefulWidget {
  @override
  _HomeScreenTopPartState createState() => _HomeScreenTopPartState();
}

final searchFieldController = TextEditingController();

List<String> locations = List();

class _HomeScreenTopPartState extends State<HomeScreenTopPart> {
  var selectedLocationIndex = 0;
  var isFlightSelected = true;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: 400.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [firstColor, secondColor]),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 50.0,
                ),
                StreamBuilder(
                  stream:
                      Firestore.instance.collection('locations').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData)
                      addLocations(context, snapshot.data.documents);
                    return !snapshot.hasData
                        ? Container()
                        : Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 16.0,
                                ),
                                PopupMenuButton(
                                  onSelected: (index) {
                                    setState(() {
                                      selectedLocationIndex = index;
                                    });
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        locations[selectedLocationIndex],
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                      Icon(Icons.keyboard_arrow_down,
                                          color: Colors.white),
                                    ],
                                  ),
                                  itemBuilder: (BuildContext context) =>
                                      buildPopUpMenuItem(),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.settings,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          );
                  },
                ),
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  'Where would\n you want to go?',
                  style: TextStyle(fontSize: 24.0, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 32.0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    child: TextField(
                      controller: searchFieldController,
                      style: dropDownMenuItemStyle,
                      cursorColor: appTheme.primaryColor,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 32.0, vertical: 14.0),
                        suffixIcon: Material(
                          elevation: 2.0,
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            InheritedFlightListing(
                                              fromLocation: locations[
                                                  selectedLocationIndex],
                                              toLocation:
                                                  searchFieldController.text,
                                              child: FlightListingScreen(),
                                            )));
                              },
                              child: Icon(
                                Icons.search,
                                color: Colors.black,
                              )),
                        ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    InkWell(
                      // child: ChoiceChip(
                      //     Icons.flight_takeoff, "Flights", isFlightSelected),
                      child: MyChoiceChip(
                          Icons.flight_takeoff, "Flights", isFlightSelected),
                      onTap: () {
                        setState(() {
                          isFlightSelected = true;
                        });
                      },
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    InkWell(
                      child: MyChoiceChip(
                          Icons.hotel, "Hotels", !isFlightSelected),
                      onTap: () {
                        setState(() {
                          isFlightSelected = false;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class Location {
  final String name;

  Location.fromMap(Map<String, dynamic> map)
      : assert(map['name'] != null),
        name = map['name'];

  Location.fromSnapShot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data);
}

List<PopupMenuItem<int>> buildPopUpMenuItem() {
  List<PopupMenuItem<int>> popUpMenuItems = List();
  for (int i = 0; i < locations.length; i++) {
    popUpMenuItems.add(PopupMenuItem(
        child: Text(
      locations[i],
      style: dropDownMenuItemStyle,
    )));
  }
  return popUpMenuItems;
}

addLocations(BuildContext context, List<DocumentSnapshot> snapshot) {
  for (int i = 0; i < snapshot.length; i++) {
    final Location location = Location.fromSnapShot(snapshot[i]);
    locations.add(location.name);
  }
}
