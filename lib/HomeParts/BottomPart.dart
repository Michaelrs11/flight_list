import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flightlist/Cards/CityCard.dart';
import 'package:flightlist/ScreenParts/HomeScreen.dart';
import 'package:flutter/material.dart';

var viewAllStyle = TextStyle(fontSize: 14.0, color: appTheme.primaryColor);

var homeScreenBottomPart = Column(
  children: <Widget>[
    Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Currently watched Items",
            style: dropDownMenuItemStyle,
          ),
          Spacer(),
          Text(
            "VIEW ALL(12)",
            style: viewAllStyle,
          ),
        ],
      ),
    ),
    Container(
      height: 210.0,
      child: StreamBuilder(
        stream: Firestore.instance.collection('cities').snapshots(),
        builder: (context, snapshot){
          print('${snapshot.hasData}');
          return !snapshot.hasData
            ? Center(child: CircularProgressIndicator())
            : buildCitiesList(context, snapshot.data.documents);
        } ,
      ),
    ),
  ],
);

Widget buildCitiesList(BuildContext context, List<DocumentSnapshot> snapshot){
  return ListView.builder(
    itemCount: snapshot.length,
    scrollDirection: Axis.horizontal,
    itemBuilder: (context,index){
      return CityCard(city: City.fromSnapShot(snapshot[index]),);
    },
  );
}
